package tests.pages.google;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static tests.common.CommonSteps.checkUrl;
import static tests.common.CommonSteps.getAndAttachScreenshot;
import static tests.logger.CustomLogger.logger;

public class GoogleSearchResultsPage {
    String searchResultXPath = "//h3//span";

    @Step
    public void checkSearchResultsEquals(String text) {
        checkUrl("search");
        $(byXpath(searchResultXPath + "[text()='" + text + "']")).shouldBe(Condition.visible);
        getAndAttachScreenshot();
        logger.info("value: " + text + " - ok");
    }

    @Step
    public void checkSearchResultsContains(String text) {
        checkUrl("search");
        $(byXpath(searchResultXPath + "[contains(text(), '" + text + "')]")).shouldBe(Condition.visible);
        getAndAttachScreenshot();
        logger.info("value: " + text + " - ok");
    }

}

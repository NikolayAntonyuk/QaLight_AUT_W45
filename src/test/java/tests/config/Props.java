package tests.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Props {

    //путь к нашему файлу конфигураций
    public static final String PATH_TO_PROPERTIES = "src/test/resources/config.properties";
    public static String site = "";
    public static String loginToSite = "";
    public static String passwordToSite = "";

    public static void initProperties() {
        FileInputStream fileInputStream;
        //инициализируем специальный объект Properties
        //типа Hashtable для удобной работы с данными
        Properties prop = new Properties();
        try {
            //обращаемся к файлу и получаем данные
            fileInputStream = new FileInputStream(PATH_TO_PROPERTIES);
            prop.load(fileInputStream);
            site = prop.getProperty("site");
            loginToSite = prop.getProperty("login");
            passwordToSite = prop.getProperty("password");
            //печатаем полученные данные в консоль
            System.out.println(
                    "site: " + site
                            + "\nloginToSite: " + loginToSite
                            + "\npasswordToSite: " + passwordToSite
            );
        } catch (IOException e) {
            System.out.println("Ошибка в программе: файл " + PATH_TO_PROPERTIES + " не обнаружено");
            e.printStackTrace();
        }
    }

    public static void resetProperties() {
        site = "";
        loginToSite = "";
        passwordToSite = "";
    }

}

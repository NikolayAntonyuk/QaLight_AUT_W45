package tests.ui_tests.google;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import tests.data.Projects;
import tests.runners.Debug;
import tests.utils.ExcelUtils;

import static com.codeborne.selenide.Selenide.open;

public class GoogleTests extends Debug {

    @DataProvider
    public Object[][] testObjArray1() {
        return ExcelUtils.getTableArray("src//test//resources//test_data//TestData.xlsx", "Sheet1");
    }

    @Test(dataProvider = "testObjArray1")
    public void test1(String var1, String var2) {
        System.out.println("var1: " + var1);
        System.out.println("var2: " + var2);
    }

    @Test(groups = "checkGoogleSearch1")
    public void checkGoogleSearch1() {
        open(Projects.GOOGLE_MAIN_PAGE.getUrl());
        googleMainPage.fillTheFieldSearch("Kolya hello!!!");
        googleMainPage.pressButtonEnter();
        googleSearchResultsPage.checkSearchResultsContains("Kolya");
    }

    @Test(groups = "checkGoogleSearch2")
    public void checkGoogleSearch2() {
        open(Projects.GOOGLE_MAIN_PAGE.getUrl());
        googleMainPage.fillTheFieldSearch("Kolya how are you!!!");
        googleMainPage.pressButtonEnter();
        googleSearchResultsPage.checkSearchResultsContains("Kolya");
    }

    @Test(groups = "checkGoogleSearch3")
    public void checkGoogleSearch3() {
        open(Projects.GOOGLE_MAIN_PAGE.getUrl());
        googleMainPage.fillTheFieldSearch("Kolya bye!!!");
        googleMainPage.pressButtonEnter();
        googleSearchResultsPage.checkSearchResultsContains("Kolya");
    }

    @Test(groups = "checkGoogleSearch4")
    public void checkGoogleSearch4() {
        open(Projects.GOOGLE_MAIN_PAGE.getUrl());
        googleMainPage.fillTheFieldSearch("Lesha hello!!!");
        googleMainPage.pressButtonEnter();
        googleSearchResultsPage.checkSearchResultsContains("Lesha");
    }

}

package tests.runners;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.codeborne.selenide.testng.ScreenShooter;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import tests.pages.BasePage;

import static tests.config.Props.initProperties;
import static tests.config.Props.resetProperties;

public class Debug extends BasePage {

    @Parameters({"browserName"})
    @BeforeMethod(alwaysRun = true)
    public void setUp(@Optional String browserName) {
        if (browserName != null) {
            Configuration.browser = browserName;
        }
        Configuration.startMaximized = true;
        Configuration.reportsFolder = "target/screenshots";
        Configuration.timeout = 10000;
        ScreenShooter.captureSuccessfulTests = true;
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(true));
        initProperties();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() {
        Selenide.closeWebDriver();
        resetProperties();
    }

}
